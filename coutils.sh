#!/bin/dash


result=""
maxlen=10


case $1 in
     top_io)
       maxlen=30
       if [ "$2" = "name" ]
       then
         maxlen=12
         data=$(sudo iotop -b -d 0.01 -n1  | awk 'NR==4{print $12}'| tr -d '\n')
       elif [ "$2" = "top" ]
       then
         maxlen=30
         data=$(sudo iotop -b -d 0.01 -n1  | awk 'NR==4{print $1, "r:"$4 $5, "w:"$6 $7}'| tr -d '\n')
       fi
       result=$data
       ;;
     pidactivewindow)
          maxlen=9
          pid=$( xprop -id `xdotool getwindowfocus` | grep '_NET_WM_PID' | grep -oE '[[:digit:]]*$' )
	  if [ "$?" != "0" ]
          then
            result="no data"
          else
            result=$pid'pid '$(ps u $pid| awk '{print $3 "cpu", $4 "mem"}'|tail -n1)
          fi
          echo $result
          result="NO_PRINT"
          ;;
     nameactivewindow)
          maxlen=45
          pid=$( xprop -id `xdotool getwindowfocus` | grep '_NET_WM_PID' | grep -oE '[[:digit:]]*$' )
          if [ "$?" != "0" ]
          then
            name="no name"
          else
            name=$(ps u $pid| tail -n1|tr -s ' '|cut -d ' ' -f11|grep -Po "[[:alnum:][:graph:]]{1,40}$")
          fi
          result=$name
          ;;
     cputemp)
          temp=$( cat /sys/class/hwmon/hwmon1/temp1_input )
          result=$( echo $temp|cut -c 1-$(( $(expr length $temp)-3 )) )
          ;;
     btcusd)
          data=$( timeout 1 wget --timeout=1 -q -O - http://blockchain.info/ticker| grep "USD"|cut -d',' -f3|cut -d' ' -f4)
          if [ "$?" = "0" ]
          then
            result=$data
          else
            result=0
          fi
          ;;
     nvtemp)
          result=$( nvidia-smi --query-gpu=temperature.gpu --format=csv,noheader,nounits  )
          ;;
     cpuname)
          maxlen=16
          result=$( cat /proc/cpuinfo | grep "model name"| head -n1|sed 's/.*: //' )
          ;;
     driveinfo)
          drive=$2
          if [ -z "$drive" ];then drive="sda";fi
          tmpfile="/tmp/coutils_driveinfo.tmp"
          echo $(sudo hdparm -I /dev/$drive| grep "Nominal Media Rotation Rate\|Advanced power management level\|Transport:"| sed 's/.*Model Number:       //g'| sed 's/.*Nominal Media.*: /Speed rate:/g'|sed 's/.*Advanced.*: /APM:/g'| sed 's/.*, SATA //g'| sed 's/Rev/SATA/g')
          result="NO_PRINT"
          ;;
     drivename)
          drive=$2
          if [ -z "$drive" ];then drive="sda";fi
          tmpfile="/tmp/coutils_driveinfo.tmp"
          sudo parted /dev/$2 p -s| grep 'Модель\|Model'| sed 's/Модель: \|Model: //g'
          #sudo hdparm -I /dev/$drive| grep "Model Number"| sed 's/.*: //g'
          result="NO_PRINT"
          ;;
     drive4kread)
          drive=$2
          byte=$(shuf -i 1-$(lsblk -b -o NAME,SIZE| grep "^$drive"| cut -f5 -d' ') -n1)
          var=$(sudo dd if=/dev/$drive of=/dev/null bs=1 count=4096  skip=$byte iflag=nofollow,nocache,noatime,sync oflag=nofollow,nocache,noatime 2>&1)
          result=$(echo ${var##}|cut -d',' -f5-6| tr -d ' s'|sed 's/,/./g')
          ;;
     drivetotalwrites)
	 #result=$(awk -v var="$2" '/var / {print "\t"$10 / 2 / 1024 / 1024}' /proc/diskstats| tr -d "[:space:]")
         #result=$(python -c "print( '%10.3f'  % (float($(grep 'sda ' /proc/diskstats| sed 's/.*sda //'| cut -d' ' -f7))/2/1024/1024))"| tr -d "[:space:]")
         num=$(grep $2' '  /proc/diskstats| tr -s ' '|cut -d' ' -f11)
         result=$(python -c "print( '%10.3f'  % (float($num)/2/1024/1024))"| tr -d "[:space:]")' GB'
         ;;
     nvmetotalread)
         result=$(sudo smartctl -A /dev/$2| grep "Data Units Read:"| cut -d'[' -f2| cut -d']' -f1)
         ;;
     nvmetotalwrite)
         result=$(sudo smartctl -A /dev/$2| grep "Data Units Written:"| cut -d'[' -f2| cut -d']' -f1)
         ;;
     nvmetemp)
         result=$(sudo smartctl -A /dev/$2| grep "Temperature Sensor 1:"| tr -s ' '| cut -d' ' -f4)
         ;;
     nvmetemp2)
         result=$(sudo smartctl -A /dev/$2| grep "Temperature Sensor 2:"| tr -s ' '| cut -d' ' -f4)
         ;;
     raminfo)
          tmpfile="/tmp/coutils_raminfo.tmp"
          sudo dmidecode --type 17 | grep "Handle \|Part Number\|Size\|Speed"|grep -v "Configured"| sed 's/, .*//g'| sed 's/.*: //g'| sed -z 's/\n/ /g'|sed 's/Handle //g'| sed 's/0x/\n0x/g'| sed 's/No Module Installed.*/Empty/g'|sed 's/-CRC//g'|sed 's/0x/ 0x/g'| grep -v '^$'
          result="NO_PRINT"
          #rm $tmpfile
          ;;
     ramname)
          maxlen=35
          tmpfile="/tmp/coutils_ramname.tmp"
          echo $(sudo dmidecode --type 17 | grep "Manufacturer\|Part Number"| grep -v "Unknown"| head -n2| sed 's/-CRC//g'| sed 's/.*: / /g') > $tmpfile
          result=$(cat $tmpfile)
          rm $tmpfile
          ;;
     mousename)
          #tmpfile="/tmp/coutils_mousename.tmp"
          #touch "$tmpfile"
          #IFS=$'\n'
          #lsusb -v 2>/dev/null| awk '/idVendor|idProduct|bInterfaceProtocol/{ print $3,$4,$5,$6 }'| tac > $tmpfile
          #for line in $(cat $tmpfile| grep -Fxn "Mouse   "| cut -d':' -f1 )
          #do
          #    name_br=$(sed -n  $(($line+2))'p' $tmpfile)
          #    if [ $name_br == "   " ];then continue;fi
          #    name_mod=$(sed -n  $(($line+1))'p' $tmpfile)
          #    result=$name_br$name_mod
          #    maxlen=35
          #done
          #if [ -f "$tmpfile" ];then rm $tmpfile;fi
          maxlen=35
          result=$(lsusb| grep "Mouse"| cut -d ' ' -f 7-)
          ;;
     keyboardname)
          #tmpfile="/tmp/coutils_keyboardname.tmp"
          #touch "$tmpfile"
          #IFS=$'\n'
          #lsusb -v 2>/dev/null| awk '/idVendor|iProduct|idProduct|bInterfaceProtocol/{ print $3,$4,$5,$6 }'| tac > $tmpfile
          #for line in $(cat $tmpfile| grep -Fxn "Keyboard   "| cut -d':' -f1 )
          #do
          #    name_br=$(sed -n  $(($line+2))'p' $tmpfile)
          #    if [ $name_br == "   " ];then continue;fi
          #    name_mod=$(sed -n  $(($line+1))'p' $tmpfile)
          #    result=$name_br$name_mod
          #    maxlen=35
          #done
          #if [ -f "$tmpfile" ];then rm $tmpfile;fi
          maxlen=35
          result=$(lsusb| grep "Keyboard"| cut -d ' ' -f 7-)
          ;;
     net_name)
          iface=$2
          if [ -e $iface ]
          then
            iface=eth0
          fi
          if [ -f /sys/class/net/$iface/device/uevent ]
          then
            result=$(lspci -nn| grep -i $(grep -s "PCI_ID" /sys/class/net/$iface/device/uevent | cut -d '=' -f2) | grep -s -oP "\]\:\ .*\ \[" | tr -d '][:' | sed 's/^ //g' | tr -s ' ' | tr -d ',.=+`~^:*$' | sed 's/Ltd\|Co\|Network\|Wireless\|Adapter//g')
          else
            result="Not device!"
          fi
          #result=$( lspci -v | grep "Ethernet controller"| sed 's/.*: //g'| sed 's/\/.*//g' )
          maxlen=42
          ;;
     nvmemusage)
          result=$( nvidia-smi --query-gpu=memory.used --format=csv,noheader,nounits  )
          ;;
     nvmemtotal)
          result=$( nvidia-smi --query-gpu=memory.total --format=csv,noheader,nounits )
          ;;
     nvmemfq)
          result=$( nvidia-smi --query-gpu=clocks.current.memory --format=csv,noheader,nounits  )
          ;;
     nvvidfq)
          result=$( nvidia-smi --query-gpu=clocks.current.video --format=csv,noheader,nounits  )
          ;;
     nvgpufq)
          result=$( nvidia-smi --query-gpu=clocks.current.graphics --format=csv,noheader,nounits  )
          ;;
     nvusage)
          result=$( nvidia-smi --query-gpu=utilization.gpu --format=csv,noheader,nounits  )
          ;;
     nvname)
          maxlen=35
          result=$( nvidia-smi --query-gpu=name --format=csv,noheader,nounits  )
          ;;
     *)
          result="error"
          ;;
esac

if [ "$result" != "NO_PRINT" ]
then
  echo $result | cut -c -$maxlen
fi

